This is a simple module that takes the search results from an ApacheSolr
search and uses them to generate a CSV file. The CSV file and a basic
control (on / off checkbox) is displayed in a custom block that must be
placed on the search page. When the box is checked, all searches result
in a new CSV. The files are saved in the default/files/ folder and are
named with an MD5 hash of the ApacheSolr Request string used to generate
the search results.

The results saved into the CSV aren't just what you see on the page,
they are ALL results for the query. Because of this, some queries that
return large numbers of results can kill the server. That's why the
checkbox was implemented. To further aid the user, I threw in some
simple code that displays the total number of search results next to the
search field after a search has been performed. I've also extended the
PHP time limit for the function that does all of the work.

I'm hoping that, with the help of others, the above problem can be
mitigated more gracefully. I'd also love to see some new features added
to this, like permissions control, for example.

Please test, use, and enjoy!

thanks,

Sunil
